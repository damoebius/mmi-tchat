var fs = require('fs');
var Client = require('ssh2').Client;

console.log("Executing SSH command");

var conn = new Client();
var connectConfig = {
    host: "vps.tamina-online.com",
    port: 22,
    username: "tamina",
    password:  process.argv[2]
};


conn.on('ready', function() {
    console.log('SSH client ready');

    var command = "cd ~/projects/mmi-tchat && forever stop src/server.js && git pull && npm install && forever start src/server.js";

    console.log("Executing: " + command);

    conn.exec(command, function(err, stream) {
        if (err) {
            throw err;
        }

        stream.on('data', function(data, stderr) {
            if (stderr) {
                console.log('STDERR: ' + data);
            } else {
                console.log(''+data);
            }
        }).on('exit', function(code, signal) {
            console.log('Exited with code ' + code);
            process.exit();
        });
    });
}).connect(connectConfig);
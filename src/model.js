class Message{
    constructor(user,text,date){
        this.user = user;
        this.text = text;
        this.date = new Date();
        this.id = this.date.getTime();
    }
}

class User{
    constructor(name){
        this.name = name;
    }
}

module.exports.Message = Message;
module.exports.User = User;
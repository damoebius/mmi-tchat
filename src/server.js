console.log("start server");

const express = require('express')
const bodyParser = require('body-parser');
const service = require('./mock-services.js').Mock;
const Message = require('./model.js').Message;
const User = require('./model.js').User;

const server = express()

server.get('/', function (req, res) {
  res.send('Hello World!')
})

server.use('/clients',express.static('src/clients'));
server.use(bodyParser.json());


server.listen(3000, function () {
  console.log('Listening on port 3000!');
})



server.get('/api/messages', function (req, res) {
    console.log('Get All messages');
    res.status(200).send( service.getMessages());
});

server.get('/api/messages/:id', function (req, res) {
    var id = req.params.id;
    console.log('Get messages from ' + isNaN(parseInt(id)));
  if (isNaN(parseInt(id))) {
    res.status(400).send('ID must be a valid integer');
  } else {
    res.status(200).send(service.getMessagesFrom(id));
  }
});

server.post('/api/messages', function (req, res) {
    console.log('add message');
    service.addMessage(new Message(new User(req.body.user.name),req.body.text))
    res.status(200).send({});
});
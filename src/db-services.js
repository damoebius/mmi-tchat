const Message = require('./model.js').Message;
const User = require('./model.js').User;
const MongoClient = require('mongodb').MongoClient;

class DBServices{
    constructor(){
        const dbName = 'myproject';
        const url = 'mongodb://localhost:27017';
        MongoClient.connect(url, function(err, client) {
            assert.equal(null, err);
            console.log("Connected successfully to server");
           
            const db = client.db(dbName);
           
            client.close();
          });
    }

    getMessages(){
        return [];
    }
    getMessagesFrom(id){
        return [];
    }

    addMessage(message){
        
    }
}

module.exports.DBServices = new DBServices();
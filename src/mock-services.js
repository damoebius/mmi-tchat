const Message = require('./model.js').Message;
const User = require('./model.js').User;

class Mock{
    constructor(){
        this.users = [];
        this.users.push(new User('David'));
        this.users.push(new User('Martine'));
        this.messages = [];
        this.messages.push(new Message(this.users[0],'coucou'));
        this.messages.push(new Message(this.users[1],'Bonjour David'));
        this.messages.push(new Message(this.users[0],'Tu vas bien ?'));
    }

    getMessages(){
        return this.messages;
    }
    getMessagesFrom(id){
        return this.messages;
    }

    addMessage(message){
        this.messages.push(message);
    }
}

module.exports.Mock = new Mock();